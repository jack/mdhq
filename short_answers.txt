Short Technical Communication Questions

1. What does O(n) mean? O(n^2)?

O(n) is a shorthand representation for a linear progression. As a generalized
form of f(x), it tells us that as x approaches infinity, the time or
computations required to solve f(x) will increase in a linear fashion. As x
increases, the time/computations required will increase in a one-to-one
relationship with x. O(n^2), on the other hand, will grow exponentially by the
power of 2. As x increases, the time or computations required can be
represented by x^2. This indicates that performance of the function will
decrease significantly as it scales.

2. Describe the quick sort algorithm.

The quick sort algorithm works by selecting a member of an array to use as an
arbitary 'pivot point', and then goes through all other elements of the array
and sorts them to the greater than or less than 'side' of the pivot in the
array. It's recursive, and after this initial sort will then subsequently sort
the two halves just created by the initial sort, ad infinitum until the entire
array is sorted.

3. In C, what is a pointer?

A C pointer is a variable which points to a particular memory address. Rather
than directly returning the contents stored at an address as a variable would,
it instead returns the address itself. Because different types of data are
stored using different size allocations, pointers also are assigned a type. A
pointer can function as a sort of position head, moving through a string,
array, etc.

4. What is the difference between Stack and Heap memory? Both the stack and
heap are stored in memory. Stack memory functions as a stack, or 'last in,
first out' model. As functions are called, variables set within functions, and
other thread-specific actions occur, the associated data is set sequentially on
the stack. As functions exit and data specific to the function is no longer
required, the stack is 'rolled back'. This makes it fast and efficient. Heap
memory, on the other hand, is allocated in no particular order. It is assigned
to the process running the application, and is used to store more permanent or
larger data. Chunks of memory are assigned as requested. Because it is not
stored sequentially, working with it is more complex and time-consuming.

5. What is the purpose of a garbage collector?

A garbage collector frees memory associated with objects which no longer exist.
In a language without garbage collection, it is the responsibility of the
programmer to ensure that when an object is no longer in use the section of
memory in which the object was stored is freed for future use. As an
application increases in complexity, it becomes increasingly challenging to
ensure that all unused memory has been freed. The purpose of the garbage
collector is to allow whe programmer to no longer concern him- or herself with
managing memory, at the cost of incurring some additional overhead to run the
automated garbage collection system.
