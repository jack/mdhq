README for solve, Sudoku puzzle solver

Notes:

My solution, written in Ruby, reads input from STDIN until it determines that
it has received 9 lines of input, at which points it attempts to solve the
puzzle. Upon solving it, the completed board is rendered.

1. How does your algorithm work?

My algorithm works by working through the puzzle cell-by-cell, beginning in the
upper left-hand corner. If the cell is already filled in (part of the original
board), it will verify that the rest of the board is not incompatible with the
value in the cell, and recursively call itself on the next cell, assuming that
the current cell is not the last.

If the cell is blank, it will attempt to fill in numbers, beginning at 1 and
increasing to 9, checking the row, column, and section at each number to ensure
it works. Unless it is the last cell of board, it will then call itself on the
next cell. Assuming that this is the last cell of the board (the lower right
cell), the algorithm will call a function to verify the entire board, and
return true.

If the algorithm gets stuck anywhere, it will return false, and the next
previous cell will continue iterating 1-9 until it can proceed. If cells
continue to be unsolvable, the algorithm will return false all the way back to
the beginning.

2. Give and explain the big-oh notation of the best, worst, and expected
run-time of your program.

I'm not an expert at big O notation, but I would think that in the best case,
the program would run at O(n). That's assuming that the first number attempted
in a given square is correct. It is extremely unlikely that this would occur.
In the worst case, I think it would be something on the order of O(n^n),
because it could attempt almost every number in almost every square almost as
many times as there are squares before finding a solution. Typically, I'm
guessing it's probably something like O(4n), based on looking at the iterations
per solution.

3. Why did you design your program the way you did?

Because I was designing the program to solve a specific case of 9x9 Sudoku
puzzle, a recursive trial-and-error approach seemed easiest. I benchmarked the
program on my machine as being capable of solving 99 puzzles in ~18 seconds.
For practical purposes, this seemed adequate, and pursuing alternative
solutions seemed much more complex.

4. What are some other decisions you could have made. How might they have been
better? How might they have been worse?

I think you might speed up this program by tracking numbers already used in a
given row/column/section. Another class managing stacks of used numbers in a
row/column/section, with numbers being added and removed to each stack as the
puzzle progressed, would avoid having to check the validity of each number as
the puzzle progressed. However, the overhead of managing the stacks could be
less efficient, and would certainly consume more memory.

If you needed to solve a LOT of Sudoku problems quickly, it might be worth
generating some sort of trie which could narrow down options given the seed
numbers, and without having to check so many rows/columns.

My program runs a final sanity check at the end of the puzzle which verifies
every row, column, and section in the puzzle for validity. In theory, this
should be unnecessary if the algorithm did its job correctly, but I always like
to be sure. Removing this would speed it up.
